

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.gitlab.io/hacktivistes/aaron-swartz/rss.xml>`_

.. _aaron_swartz:

==========================
|aaron| **Aaron Swartz**
==========================

- https://fr.wikipedia.org/wiki/Aaron_Swartz
- https://mastodon.social/@AaronSwartzDay
